package com.demo2.library;

import org.springframework.data.jpa.repository.JpaRepository;
import com.demo2.model.Library;

public interface LibraryRepository extends JpaRepository<Library, Integer>{  
}